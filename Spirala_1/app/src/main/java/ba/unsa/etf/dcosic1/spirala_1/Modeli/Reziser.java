package ba.unsa.etf.dcosic1.spirala_1.Modeli;


import android.os.Parcel;
import android.os.Parcelable;

public class Reziser implements Parcelable {
    private String imePrezime;

    private int imdb;

    public int getImdb() {
        return imdb;
    }

    public void setImdb(int imdb) {
        this.imdb = imdb;
    }

    private Reziser(Parcel in) {
        imePrezime = in.readString();
    }

    public static final Creator<Reziser> CREATOR = new Creator<Reziser>() {
        @Override
        public Reziser createFromParcel(Parcel in) {
            return new Reziser(in);
        }

        @Override
        public Reziser[] newArray(int size) {
            return new Reziser[size];
        }
    };

    public String getImePrezime() {
        return imePrezime;
    }

    public void setImePrezime(String imePrezime) {
        this.imePrezime = imePrezime;
    }

    public Reziser(String imePrezime) {
        this.imePrezime = imePrezime;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(imePrezime);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj==null) return false;
            Reziser rez = (Reziser)obj;
            return rez.getImePrezime().equals(this.getImePrezime());
    }
}
