
package ba.unsa.etf.dcosic1.spirala_1.Fragmenti;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import java.util.Locale;

import ba.unsa.etf.dcosic1.spirala_1.R;


public class ButtonFragment extends Fragment {

    private onGlumciClicked oic;
    private onZanroviClicked oiz;
    private onReziseriClicked oir;
    private Button dugmeGlumci;
    private Button dugmeReziseri;
    private Button dugmeZanrovi;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        String locale = Locale.getDefault().getDisplayLanguage();

        View newview= inflater.inflate(R.layout.fragment_button, container, false);

        ImageView icon = (ImageView)newview.findViewById(R.id.zastava);

        if (icon!=null)
        if (locale.equals("English")) icon.setImageResource(R.drawable.en);
        else if (locale.equals("Bosnian")) icon.setImageResource(R.drawable.ba);

        return newview;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

        dugmeGlumci = (Button) getActivity().findViewById(R.id.buttonGlumci);
        dugmeReziseri = (Button) getActivity().findViewById(R.id.buttonReziseri);
        dugmeZanrovi = (Button) getActivity().findViewById(R.id.buttonZanrovi);

       if (dugmeGlumci!=null)
       {
           dugmeGlumci.setBackgroundColor(Color.GREEN);
           dugmeZanrovi.setBackgroundColor(Color.GRAY);
           dugmeReziseri.setBackgroundColor(Color.GRAY);

           try {
               oic = (ButtonFragment.onGlumciClicked) getActivity();
               oiz = (ButtonFragment.onZanroviClicked) getActivity();
               oir = (ButtonFragment.onReziseriClicked) getActivity();

           } catch (ClassCastException e​) {
               throw new ClassCastException(getActivity().toString() + "Treba implementirati onListItemClick");
           }

           dugmeGlumci.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {
                   dugmeGlumci.setBackgroundColor(Color.GREEN);
                   dugmeZanrovi.setBackgroundColor(Color.GRAY);
                   dugmeReziseri.setBackgroundColor(Color.GRAY);
                   oic.onGlumciClick();
               }
           });

           dugmeZanrovi.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {
                   dugmeGlumci.setBackgroundColor(Color.GRAY);
                   dugmeZanrovi.setBackgroundColor(Color.GREEN);
                   dugmeReziseri.setBackgroundColor(Color.GRAY);
                   oiz.onZanroviClick();
               }
           });

           dugmeReziseri.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    dugmeGlumci.setBackgroundColor(Color.GRAY);
                                                    dugmeZanrovi.setBackgroundColor(Color.GRAY);
                                                    dugmeReziseri.setBackgroundColor(Color.GREEN);
                                                    oir.onReziseriClick();
                                                }
                                            }
           );
       }
    }

    public interface onGlumciClicked
    {
        void onGlumciClick();
    }

    public interface onZanroviClicked
    {
         void onZanroviClick();
    }

    public interface onReziseriClicked
    {
         void onReziseriClick();
    }

}
