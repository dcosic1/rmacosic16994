package ba.unsa.etf.dcosic1.spirala_1.Fragmenti;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import ba.unsa.etf.dcosic1.spirala_1.Adapteri.ReziserAdapter;
import ba.unsa.etf.dcosic1.spirala_1.AsyncTaskovi.ActorDetails;
import ba.unsa.etf.dcosic1.spirala_1.AsyncTaskovi.MovieDetails;
import ba.unsa.etf.dcosic1.spirala_1.BazaHelperi.GlumacDBOpenHelper;
import ba.unsa.etf.dcosic1.spirala_1.Modeli.Glumac;
import ba.unsa.etf.dcosic1.spirala_1.Modeli.Movie;
import ba.unsa.etf.dcosic1.spirala_1.Modeli.Reziser;
import ba.unsa.etf.dcosic1.spirala_1.R;

public class ReziseriFragment extends Fragment implements MovieDetails.onMovieDone, ActorDetails.onArtistDone{

    private ArrayList<Reziser> reziseri;
    private ListView listaReziseri;
    private ReziserAdapter adapter;
    private int size;
    private GlumacDBOpenHelper db;
    private View v;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View newView = inflater.inflate(R.layout.fragment_reziseri, container, false);
        this.v=newView;
        reziseri  = new ArrayList<>();
        listaReziseri = (ListView) newView.findViewById(R.id.listaReziseri);
        adapter = new ReziserAdapter(newView.getContext(), reziseri);
        listaReziseri.setAdapter(adapter);

        db = new GlumacDBOpenHelper(newView.getContext());

        Toast.makeText(newView.getContext(), String.valueOf(db.sviGlumci().size()), Toast.LENGTH_LONG).show();

        if (getArguments()!=null && getArguments().containsKey("glumac"))
        {
           int id=getArguments().getInt("glumac");
            Boolean baza = getArguments().getBoolean("baza");
            if (!baza)
            new ActorDetails(ReziseriFragment.this).execute(id);
            else
            {
                reziseri.addAll(db.dajRezisereGlumca(id));
                adapter.notifyDataSetChanged();
                Toast.makeText(newView.getContext(), "iz baze cita", Toast.LENGTH_LONG).show();
            }
        }
        return newView;
    }

    @Override
    public void OnMovieDone(Movie m) {
        if (reziseri.size()<size-1)
        {
            if (!reziseri.contains(m.getReziser()))
            reziseri.add(m.getReziser());
            else size--;
        }
        else {
            if (!reziseri.contains(m.getReziser()))
                reziseri.add(m.getReziser());
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onDone(Glumac rez) {
        size=rez.getMovieID().size();
        for (int i=0; i<rez.getMovieID().size(); i++)
        {
            new MovieDetails(ReziseriFragment.this).execute(rez.getMovieID().get(i));
       }

    }
}
