package ba.unsa.etf.dcosic1.spirala_1.AsyncTaskovi;


import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

import ba.unsa.etf.dcosic1.spirala_1.Modeli.Movie;

public class SearchMovie extends AsyncTask<String, Integer, Void> {

    ArrayList<Movie> movieArrayList;

    public interface onMovieSearchDone
    {
        void onDone (ArrayList<Movie> rez);
    }

    private onMovieSearchDone pozivatelj;

    public SearchMovie(onMovieSearchDone p) {pozivatelj=p;}

    @Override
    protected Void doInBackground(String... strings) {
        movieArrayList = new ArrayList<>();
        String query = null;

        try {
            query = URLEncoder.encode(strings[0], "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String url1 = "https://api.themoviedb.org/3/search/movie?api_key=3d8300df632dafd9b3bae72ed7fa946b&query=" + query;

        try {
            URL url = new URL(url1);
            HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String rezultat = convertInputToString(in);

            JSONObject jsonObject = new JSONObject(rezultat);

            JSONArray glumci = jsonObject.getJSONArray("results");

            for (int i = 0; i < glumci.length(); i++) {
                JSONObject glumacJson = glumci.getJSONObject(i);
                Movie movie = new Movie();
                movie.setId(glumacJson.getInt("id"));
                movie.setNaziv(glumacJson.getString("title"));
                movieArrayList.add(movie);
            }

        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String convertInputToString(InputStream in) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        pozivatelj.onDone(movieArrayList);
    }
}
