package ba.unsa.etf.dcosic1.spirala_1.Adapteri;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ba.unsa.etf.dcosic1.spirala_1.R;
import ba.unsa.etf.dcosic1.spirala_1.Modeli.Reziser;


public class ReziserAdapter extends ArrayAdapter<Reziser> {
    private int resource;

    public ReziserAdapter(Context context, ArrayList<Reziser> objects){
        super(context, R.layout.element_liste_reziseri, objects);
        this.resource= R.layout.element_liste_reziseri;
        Context context1 = context;
        List<Reziser> listaZanrova = objects;
    }

    public View getView(int position, View convertView, ViewGroup parent){

        LinearLayout newView;
        if (convertView == null) {

            newView = new LinearLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater li;
            li = (LayoutInflater)getContext().
                    getSystemService(inflater);
            li.inflate(resource, newView, true);
        } else {
            newView = (LinearLayout)convertView;
        }

       Reziser zanr = getItem(position);

        TextView naziv = (TextView)newView.findViewById(R.id.reziser_naziv);


        if (zanr != null) {
            naziv.setText(zanr.getImePrezime());
        }

        return newView;
    }



}