package ba.unsa.etf.dcosic1.spirala_1.Fragmenti;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import ba.unsa.etf.dcosic1.spirala_1.Adapteri.ZanrAdapter;
import ba.unsa.etf.dcosic1.spirala_1.AsyncTaskovi.ActorDetails;
import ba.unsa.etf.dcosic1.spirala_1.AsyncTaskovi.MovieDetails;
import ba.unsa.etf.dcosic1.spirala_1.BazaHelperi.GlumacDBOpenHelper;
import ba.unsa.etf.dcosic1.spirala_1.Modeli.Glumac;
import ba.unsa.etf.dcosic1.spirala_1.Modeli.Movie;
import ba.unsa.etf.dcosic1.spirala_1.Modeli.Zanr;
import ba.unsa.etf.dcosic1.spirala_1.R;


public class ZanroviFragment extends Fragment implements MovieDetails.onMovieDone, ActorDetails.onArtistDone {

    private ArrayList<Zanr> zanrovi;
    private ListView listaZanrovi;
    private ZanrAdapter adapter;
    private int size;

    private GlumacDBOpenHelper db;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View newview =inflater.inflate(R.layout.fragment_zanrovi, container, false);

        db = new GlumacDBOpenHelper(newview.getContext());

        zanrovi=new ArrayList<>();
        listaZanrovi = (ListView) newview.findViewById(R.id.listaZanrovi);
        adapter = new ZanrAdapter(newview.getContext(), zanrovi);
        listaZanrovi.setAdapter(adapter);

        if (getArguments()!=null && getArguments().containsKey("glumac"))
        {
            int id = getArguments().getInt("glumac");
            Boolean baza = getArguments().getBoolean("baza");
            if (!baza)
            new ActorDetails((ActorDetails.onArtistDone)ZanroviFragment.this).execute(id);
            else
            {
                zanrovi.addAll(db.dajZanroveGlumca(id));
                adapter.notifyDataSetChanged();
                Toast.makeText(newview.getContext(), "iz baze cita", Toast.LENGTH_LONG).show();

            }
        }
        return newview;
    }

    @Override
    public void OnMovieDone(Movie m) {
        if (zanrovi.size()<size-1)
        {
            if (zanrovi.contains(m.getZanr()))
            size--;
            else zanrovi.add(m.getZanr());
        }
        else {
            if (!zanrovi.contains(m.getZanr()))
                zanrovi.add(m.getZanr());
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onDone(Glumac rez) {
        size=rez.getMovieID().size();
        for (int i=0; i<rez.getMovieID().size(); i++)
        {
            new MovieDetails((MovieDetails.onMovieDone)ZanroviFragment.this).execute(rez.getMovieID().get(i));
        }
    }

}
