package ba.unsa.etf.dcosic1.spirala_1.Fragmenti;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import ba.unsa.etf.dcosic1.spirala_1.R;


public class ButtonHorizontalFragment extends Fragment {

    private onOthersButtonClicked iob;
    private onGlumciButtonClicked iog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       View newView= inflater.inflate(R.layout.fragment_button_horizontal, container, false);

        iob=(onOthersButtonClicked)getActivity();
        iog=(onGlumciButtonClicked)getActivity();

        Button ostali = (Button)newView.findViewById(R.id.ostali);
        Button glumci = (Button)newView.findViewById(R.id.glumci);

        ostali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iob.OnOtherButtonClick();
            }
        });

        glumci.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iog.onGlumciButtonClick();
            }
        });

        return newView;
    }

    public interface onOthersButtonClicked
    {
        void OnOtherButtonClick();
    }

    public interface onGlumciButtonClicked
    {
        void onGlumciButtonClick();
    }
}
