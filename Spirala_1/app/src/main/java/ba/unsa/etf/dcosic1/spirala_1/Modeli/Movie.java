package ba.unsa.etf.dcosic1.spirala_1.Modeli;


public class Movie {

    private String naziv;
    private int id;
    private Zanr zanr;
    private Reziser reziser;

    public Zanr getZanr() { return zanr; }

    public void setZanr(Zanr zanr) {
        this.zanr = zanr;
    }

    public Reziser getReziser() {
        return reziser;
    }

    public void setReziser(Reziser reziser) {
        this.reziser = reziser;
    }

    public String getNaziv() { return naziv; }

    public void setNaziv(String naziv) { this.naziv = naziv; }

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }
}
