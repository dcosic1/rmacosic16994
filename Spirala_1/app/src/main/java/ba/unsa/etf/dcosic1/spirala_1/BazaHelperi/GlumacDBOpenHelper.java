package ba.unsa.etf.dcosic1.spirala_1.BazaHelperi;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import ba.unsa.etf.dcosic1.spirala_1.Modeli.Glumac;
import ba.unsa.etf.dcosic1.spirala_1.Modeli.Reziser;
import ba.unsa.etf.dcosic1.spirala_1.Modeli.Zanr;

public class GlumacDBOpenHelper extends SQLiteOpenHelper {

    // Logcat tag
    private static final String LOG = "DatabaseHelper";

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "moviedb";

    // Table Names
    private static final String TABLE_GLUMCI = "glumac";
    private static final String TABLE_ZANROVI = "zanr";
    private static final String TABLE_REZISERI = "reziser";
    private static final String TABLE_GLUMCI_REZISERI = "glumac_reziser";
    private static final String TABLE_GLUMCI_ZANROVI = "glumcac_zanr";

    //  Common column names
    private static final String KEY_ID = "id";
    private static final String KEY_IMDB = "imdb";
    private static final String KEY_NAZIV = "naziv";
    private static final String KEY_GLUMAC = "glumac";

    // GLUMAC Table - column nmaes
    private static final String KEY_DATUM_RODJENJA = "datum_rodjenja";
    private static final String KEY_DATUM_SMRTI = "datum_smrti";
    private static final String KEY_LINK = "link";
    private static final String KEY_BIOGRAFIJA = "biografija";
    private static final String KEY_SPOL = "spol";
    private static final String KEY_MJESTO_RODJENJA = "mjesto_rodjenja";

    private static final String KEY_STRING = " text";
    private static final String KEY_INTEGER = " INTEGER";

    // Table Create Statements
    // Todo table create statement
    private static final String CREATE_TABLE_GLUMCI = "CREATE TABLE " + TABLE_GLUMCI + "("
            + KEY_ID + KEY_INTEGER + " PRIMARY KEY AUTOINCREMENT,"
            + KEY_IMDB + KEY_INTEGER + ","
            + KEY_NAZIV + " varchar(50),"
            + KEY_DATUM_RODJENJA + " varchar(30),"
            + KEY_DATUM_SMRTI + " varchar(30), "
            + KEY_LINK + " varchar(50), "
            + KEY_BIOGRAFIJA + KEY_STRING + ","
            + KEY_SPOL + " varchar(10), "
            + KEY_MJESTO_RODJENJA + " varchar(100)" + ")";

    private static final String CREATE_TABLE_ZANROVI = "CREATE TABLE " + TABLE_ZANROVI + "("
            + KEY_ID + KEY_INTEGER + " PRIMARY KEY AUTOINCREMENT,"
            + KEY_IMDB + KEY_INTEGER + ","
            + KEY_NAZIV + " varchar(50))";

    private static final String CREATE_TABLE_REZISERI = "CREATE TABLE " + TABLE_REZISERI + "("
            + KEY_ID + KEY_INTEGER + " PRIMARY KEY AUTOINCREMENT,"
            + KEY_IMDB + KEY_INTEGER + ","
            + KEY_NAZIV + " varchar(50))";

    private static final String CREATE_TABLE_GLUMAC_REZISERI = "CREATE TABLE " + TABLE_GLUMCI_REZISERI + "("
            + KEY_ID + KEY_INTEGER + " PRIMARY KEY AUTOINCREMENT,"
            + TABLE_GLUMCI + KEY_INTEGER + ", "
            + TABLE_REZISERI + KEY_INTEGER + ")";

    private static final String CREATE_TABLE_GLUMAC_ZANROVI = "CREATE TABLE " + TABLE_GLUMCI_ZANROVI + "("
            + KEY_ID + KEY_INTEGER + " PRIMARY KEY AUTOINCREMENT,"
            + TABLE_GLUMCI + KEY_INTEGER + ", "
            + TABLE_ZANROVI + KEY_INTEGER + ")";


    public GlumacDBOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE_GLUMCI);
        sqLiteDatabase.execSQL(CREATE_TABLE_REZISERI);
        sqLiteDatabase.execSQL(CREATE_TABLE_ZANROVI);
        sqLiteDatabase.execSQL(CREATE_TABLE_GLUMAC_REZISERI);
        sqLiteDatabase.execSQL(CREATE_TABLE_GLUMAC_ZANROVI);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GLUMCI);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ZANROVI);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_REZISERI);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GLUMCI_REZISERI);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GLUMCI_ZANROVI);

        onCreate(db);
    }

    public long DodajGlumca(Glumac glumac) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_IMDB, glumac.getId());
        contentValues.put(KEY_NAZIV, glumac.getIme());
        contentValues.put(KEY_BIOGRAFIJA, glumac.getBiografija());
        contentValues.put(KEY_DATUM_RODJENJA, glumac.getGodinaRodjenja());
        contentValues.put(KEY_DATUM_SMRTI, glumac.getGodinaSmrti());
        contentValues.put(KEY_MJESTO_RODJENJA, glumac.getMjestoRodjenja());
        contentValues.put(KEY_SPOL, glumac.getSpol());

        return db.insert(TABLE_GLUMCI, null, contentValues);
    }

    public long dodajGlumacZanr(Glumac glumac, Zanr zanr) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues;

        String selectQuery = "SELECT * FROM " + TABLE_ZANROVI + " Where " + KEY_NAZIV + " = '" + zanr.getNaziv() + "'";
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.getCount() == 0) //jos ovaj zanr nije upisan
        {
            contentValues = new ContentValues();
            contentValues.put(KEY_NAZIV, zanr.getNaziv());
            contentValues.put(KEY_IMDB, zanr.getId());
            db.insert(TABLE_ZANROVI, null, contentValues);
        }
        c.close();
        c = db.rawQuery(selectQuery, null);
        c.moveToFirst();
        int id_zanr = c.getInt(c.getColumnIndex(KEY_ID));
        c.close();

        selectQuery = "SELECT * FROM " + TABLE_GLUMCI + " Where " + KEY_IMDB + "=" + glumac.getImdb();

        c = db.rawQuery(selectQuery, null);
        c.moveToFirst();
        int id_glumac = c.getInt(c.getColumnIndex(KEY_ID));
        c.close();

        contentValues = new ContentValues();
        contentValues.put(TABLE_GLUMCI, id_glumac);
        contentValues.put(TABLE_ZANROVI, id_zanr);
        return db.insert(TABLE_GLUMCI_ZANROVI, null, contentValues);
    }

    public long dodajGlumacReziser(Glumac glumac, Reziser reziser) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues;

        String selectQuery = "SELECT * FROM " + TABLE_REZISERI + " Where " + KEY_IMDB + "=" + reziser.getImdb();
        Cursor c = db.rawQuery(selectQuery, null);


        if (c.getCount() == 0) //jos ovaj zanr nije upisan
        {
            contentValues = new ContentValues();
            contentValues.put(KEY_NAZIV, reziser.getImePrezime());
            contentValues.put(KEY_IMDB, reziser.getImdb());
            db.insert(TABLE_REZISERI, null, contentValues);
        }
        c = db.rawQuery(selectQuery, null);
        c.moveToFirst();
        int id_zanr = c.getInt(c.getColumnIndex(KEY_ID));

        selectQuery = "SELECT * FROM " + TABLE_GLUMCI + " Where " + KEY_IMDB + "=" + glumac.getImdb();

        c = db.rawQuery(selectQuery, null);
        c.moveToFirst();
        int id_glumac = c.getInt(c.getColumnIndex(KEY_ID));

        contentValues = new ContentValues();
        contentValues.put(TABLE_GLUMCI, id_glumac);
        contentValues.put(TABLE_REZISERI, id_zanr);
        return db.insert(TABLE_GLUMCI_REZISERI, null, contentValues);
    }

    public List<Glumac> sviGlumci() {
        List<Glumac> glumacs = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TABLE_GLUMCI;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        if (c.moveToFirst()) {
            do {
                Glumac td = new Glumac();
                td.setId(c.getInt((c.getColumnIndex(KEY_ID))));
                glumacs.add(td);
            } while (c.moveToNext());
        }
        return glumacs;
    }

    public ArrayList<Glumac> dajGlumca(String ime) {
        ArrayList<Glumac> glumacs = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TABLE_GLUMCI + " where naziv like '" + ime + "%'";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        if (c != null)
            if (c.moveToFirst()) {
                do {
                    Glumac td = new Glumac();
                    td.setIme(c.getString(c.getColumnIndex(KEY_NAZIV)));
                    td.setMjestoRodjenja(c.getString(c.getColumnIndex(KEY_MJESTO_RODJENJA)));
                    td.setSpol(c.getString(c.getColumnIndex(KEY_SPOL)));
                    td.setBiografija(c.getString(c.getColumnIndex(KEY_BIOGRAFIJA)));
                    td.setId(c.getInt(c.getColumnIndex(KEY_IMDB)));
                    td.setGodinaRodjenja(c.getString(c.getColumnIndex(KEY_DATUM_RODJENJA)));
                    td.setGodinaSmrti(c.getString(c.getColumnIndex(KEY_DATUM_SMRTI)));
                    glumacs.add(td);
                } while (c.moveToNext());
            }
        return glumacs;
    }

    public Glumac getGlumac(int id) {
        Glumac td = new Glumac();
        SQLiteDatabase db = this.getWritableDatabase();
        String selectQuery;

        selectQuery = "SELECT * FROM " + TABLE_GLUMCI + " Where " + KEY_IMDB + "=" + id;

        Cursor c = db.rawQuery(selectQuery, null);
        if (c.getCount() != 0) {
            c.moveToFirst();

            td.setIme(c.getString(c.getColumnIndex(KEY_NAZIV)));
            td.setMjestoRodjenja(c.getString(c.getColumnIndex(KEY_MJESTO_RODJENJA)));
            td.setSpol(c.getString(c.getColumnIndex(KEY_SPOL)));
            td.setBiografija(c.getString(c.getColumnIndex(KEY_BIOGRAFIJA)));
            td.setId(c.getInt(c.getColumnIndex(KEY_IMDB)));
            td.setGodinaRodjenja(c.getString(c.getColumnIndex(KEY_DATUM_RODJENJA)));
            td.setGodinaSmrti(c.getString(c.getColumnIndex(KEY_DATUM_SMRTI)));
        }
        else td =null;
        return td;
    }

    public ArrayList<Reziser> dajRezisereGlumca(int idGlumca) {
        ArrayList<Reziser> reziseri = new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();

        String selectQuery = "select * from " + TABLE_GLUMCI + " where " + KEY_IMDB + "=" + idGlumca;
        Cursor c = db.rawQuery(selectQuery, null);
        c.moveToFirst();
        int id = c.getInt(c.getColumnIndex(KEY_ID));


        selectQuery = "SELECT " + TABLE_REZISERI + " FROM " + TABLE_GLUMCI_REZISERI + " Where " + KEY_GLUMAC + "=" + id;

        c = db.rawQuery(selectQuery, null);

        if (c.getCount() != 0) {
            c.moveToFirst();
            do {
                int idRezisera = c.getInt(c.getColumnIndex(TABLE_REZISERI));

                selectQuery = "SELECT * FROM " + TABLE_REZISERI + " Where " + KEY_ID + "=" + idRezisera;
                Cursor k = db.rawQuery(selectQuery, null);
                if (k != null) {
                    k.moveToFirst();
                    do {
                        String imeRezisera = k.getString(k.getColumnIndex(KEY_NAZIV));
                        Reziser reziser = new Reziser(imeRezisera);
                        reziseri.add(reziser);
                    } while (k.moveToNext());
                }
            } while (c.moveToNext());
        }

        return reziseri;
    }

    public ArrayList<Zanr> dajZanroveGlumca(int idGlumca) {
        ArrayList<Zanr> reziseri = new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();

        String selectQuery = "select * from " + TABLE_GLUMCI + " where " + KEY_IMDB + "=" + idGlumca;
        Cursor c = db.rawQuery(selectQuery, null);
        c.moveToFirst();
        int id = c.getInt(c.getColumnIndex(KEY_ID));


        selectQuery = "SELECT " + TABLE_ZANROVI + " FROM " + TABLE_GLUMCI_ZANROVI + " Where " + KEY_GLUMAC + "=" + id;

        c = db.rawQuery(selectQuery, null);

        if (c.getCount() != 0) {
            c.moveToFirst();
            do {
                int idRezisera = c.getInt(c.getColumnIndex(TABLE_ZANROVI));

                selectQuery = "SELECT * FROM " + TABLE_ZANROVI + " Where " + KEY_ID + "=" + idRezisera;
                Cursor k = db.rawQuery(selectQuery, null);
                if (k != null) {
                    k.moveToFirst();
                    do {

                        String imeRezisera = k.getString(k.getColumnIndex(KEY_NAZIV));
                        Zanr zanr = new Zanr(imeRezisera);
                        reziseri.add(zanr);
                    } while (k.moveToNext());
                }


            } while (c.moveToNext());
        }

        return reziseri;
    }

    // closing database
    public void closeDB() {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen())
            db.close();
    }

    public ArrayList<Glumac> dajGlumceRezisera(String reziser)
    {
        ArrayList<Glumac> glumci = new ArrayList<>();
        String selectQuery = "SELECT * from "+TABLE_REZISERI+ " where "+KEY_NAZIV+" like '"+reziser+"%'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.getCount()!=0)
        {
            c.moveToFirst();
            int idRezisera = c.getInt(c.getColumnIndex(KEY_ID));
            selectQuery =  "SELECT * from "+TABLE_GLUMCI_REZISERI+ " where "+TABLE_REZISERI+"="+idRezisera;
            Cursor k = db.rawQuery(selectQuery, null);
            if (k.getCount()!=0)
            {
                k.moveToFirst();
                do
                {
                    int idGlumca = k.getInt(k.getColumnIndex(TABLE_GLUMCI));
                    selectQuery = " SELECT * from "+ TABLE_GLUMCI+" where "+KEY_ID+"="+idGlumca;
                    Cursor cursor = db.rawQuery(selectQuery, null);
                    if (cursor.getCount()!=0)
                    {
                        cursor.moveToFirst();
                        Glumac td= new Glumac();
                        td.setIme(cursor.getString(cursor.getColumnIndex(KEY_NAZIV)));
                        td.setMjestoRodjenja(cursor.getString(cursor.getColumnIndex(KEY_MJESTO_RODJENJA)));
                        td.setSpol(cursor.getString(cursor.getColumnIndex(KEY_SPOL)));
                        td.setBiografija(cursor.getString(cursor.getColumnIndex(KEY_BIOGRAFIJA)));
                        td.setId(cursor.getInt(cursor.getColumnIndex(KEY_IMDB)));
                        td.setGodinaRodjenja(cursor.getString(cursor.getColumnIndex(KEY_DATUM_RODJENJA)));
                        td.setGodinaSmrti(cursor.getString(cursor.getColumnIndex(KEY_DATUM_SMRTI)));
                        glumci.add(td);
                    }

                }while (k.moveToNext());
            }
        }
        return glumci;
    }

    public void obrisiGlumca(Glumac glumac)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        String selectQuery = "select * from " + TABLE_GLUMCI + " where " + KEY_IMDB + "=" + glumac.getId();
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.getCount()!=0)
        {
            c.moveToFirst();
            int id = c.getInt(c.getColumnIndex(KEY_ID));
            db.delete(TABLE_GLUMCI_REZISERI, TABLE_GLUMCI+"="+id, null);
            db.delete(TABLE_GLUMCI_ZANROVI, TABLE_GLUMCI+"="+id, null);
        }
        selectQuery = "select * from "+TABLE_ZANROVI;
        c=db.rawQuery(selectQuery, null);
        if (c.getCount()!=0)
        {   c.moveToFirst();
            do {
                int idZanra = c.getInt(c.getColumnIndex(KEY_ID));
                selectQuery="select * from "+TABLE_GLUMCI_ZANROVI+" WHERE "+TABLE_ZANROVI+"="+idZanra;
                Cursor k = db.rawQuery(selectQuery, null);
                if (k.getCount()==0)
                {
                    db.delete(TABLE_ZANROVI, KEY_ID+"="+idZanra, null);
                }
            }
            while (c.moveToNext());
        }

        selectQuery = "select * from "+TABLE_REZISERI;
        c=db.rawQuery(selectQuery, null);
        if (c.getCount()!=0)
        {   c.moveToFirst();
            do {
                int idZanra = c.getInt(c.getColumnIndex(KEY_ID));
                selectQuery="select * from "+TABLE_GLUMCI_REZISERI+" WHERE "+TABLE_REZISERI+"="+idZanra;
                Cursor k = db.rawQuery(selectQuery, null);
                if (k.getCount()==0)
                {
                    db.delete(TABLE_REZISERI, KEY_ID+"="+idZanra, null);
                }
            }
            while (c.moveToNext());
        }

        db.delete(TABLE_GLUMCI, KEY_IMDB+"="+glumac.getId(), null);

    }
}
