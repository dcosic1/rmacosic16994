package ba.unsa.etf.dcosic1.spirala_1.Modeli;


public class Zanr {

    private String naziv;
    private int id ;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public Zanr(String naziv) {
        this.naziv = naziv;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass().equals(Zanr.class))
        {
            Zanr o = (Zanr)obj;
            return this.naziv.equals(o.getNaziv());
        }
        return false;
    }
}
