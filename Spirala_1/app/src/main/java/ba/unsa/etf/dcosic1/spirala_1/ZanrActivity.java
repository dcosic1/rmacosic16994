package ba.unsa.etf.dcosic1.spirala_1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

import ba.unsa.etf.dcosic1.spirala_1.Adapteri.ZanrAdapter;
import ba.unsa.etf.dcosic1.spirala_1.Modeli.Zanr;

public class ZanrActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button dugmeGlumci = (Button) findViewById(R.id.buttonGlumci);
        final Button dugmeReziseri = (Button) findViewById(R.id.buttonReziseri);
        final Button dugmeZanrovi = (Button) findViewById(R.id.buttonZanrovi);
        final ListView listaZanrovi = (ListView) findViewById(R.id.listaGlumci);

        getSupportActionBar().setTitle("Zanrovi");



        dugmeReziseri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), ReziseriActivity.class));
            }
        });

        dugmeZanrovi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), ZanrActivity.class));
            }
        });

        dugmeGlumci.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
            }
        });

        ArrayList<Zanr> zanrovi = new ArrayList<>();
        zanrovi.add(new Zanr("Akcija"));
        zanrovi.add(new Zanr("Drama"));
        zanrovi.add(new Zanr("Komedija"));
        zanrovi.add(new Zanr("Fantazija"));
        zanrovi.add(new Zanr("Krimi"));

        ZanrAdapter adapter = new ZanrAdapter(getApplicationContext(), zanrovi);
        listaZanrovi.setAdapter(adapter);


    }
}
