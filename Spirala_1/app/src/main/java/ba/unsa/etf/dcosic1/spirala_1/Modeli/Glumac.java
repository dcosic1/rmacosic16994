package ba.unsa.etf.dcosic1.spirala_1.Modeli;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class Glumac implements Parcelable {

    private int id;
    private String ime;
    private String prezime;
    private String rating;
    private String mjestoRodjenja;
    private String godinaRodjenja;
    private String godinaSmrti;
    private String link;
    private String biografija;
    private String spol;
    private List<Integer> movieID;
    private int imdb;

    public int getImdb() {
        return imdb;
    }

    public void setImdb(int imdb) {
        this.imdb = imdb;
    }

    public List<Integer> getMovieID() {
        return movieID;
    }

    public void setMovieID(List<Integer> movieID) {
        this.movieID = movieID;
    }

    public String getSlika() {
        return slika;
    }

    public void setSlika(String slika) {
        this.slika = slika;
    }

    private String slika;

    public Glumac()
    {}


    public Glumac(String ime, String prezime, String rating, String mjestoRodjenja, String godinaRodjenja, String godinaSmrti, String link, String biografija, String spol) {
        this.ime = ime;
        this.prezime = prezime;
        this.rating = rating;
        this.mjestoRodjenja = mjestoRodjenja;
        this.godinaRodjenja = godinaRodjenja;
        this.godinaSmrti = godinaSmrti;
        this.link = link;
        this.biografija = biografija;
        this.spol = spol;
    }

    private Glumac(Parcel in) {
        ime = in.readString();
        prezime = in.readString();
        rating = in.readString();
        mjestoRodjenja = in.readString();
        godinaRodjenja = in.readString();
        godinaSmrti = in.readString();
        link = in.readString();
        biografija = in.readString();
        spol = in.readString();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ime);
        dest.writeString(prezime);
        dest.writeString(rating);
        dest.writeString(mjestoRodjenja);
        dest.writeString(godinaRodjenja);
        dest.writeString(godinaSmrti);
        dest.writeString(link);
        dest.writeString(biografija);
        dest.writeString(spol);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Glumac> CREATOR = new Creator<Glumac>() {
        @Override
        public Glumac createFromParcel(Parcel in) {
            return new Glumac(in);
        }

        @Override
        public Glumac[] newArray(int size) {
            return new Glumac[size];
        }
    };

    public String getGodinaSmrti() {
        return godinaSmrti;
    }

    public void setGodinaSmrti(String godinaSmrti) {
        this.godinaSmrti = godinaSmrti;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getBiografija() {
        return biografija;
    }

    public void setBiografija(String biografija) {
        this.biografija = biografija;
    }

    public String getSpol() {
        return spol;
    }

    public void setSpol(String spol) {
        this.spol = spol;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getMjestoRodjenja() {
        return mjestoRodjenja;
    }

    public void setMjestoRodjenja(String mjestoRodjenja) {
        this.mjestoRodjenja = mjestoRodjenja;
    }

    public String getGodinaRodjenja() {
        return godinaRodjenja;
    }

    public void setGodinaRodjenja(String godinaRodjenja) {
        this.godinaRodjenja = godinaRodjenja;
    }
}
