package ba.unsa.etf.dcosic1.spirala_1.Fragmenti;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

import ba.unsa.etf.dcosic1.spirala_1.Adapteri.MovieAdapter;
import ba.unsa.etf.dcosic1.spirala_1.AsyncTaskovi.SearchMovie;
import ba.unsa.etf.dcosic1.spirala_1.Modeli.Movie;
import ba.unsa.etf.dcosic1.spirala_1.R;


public class SearchMovieFragment extends Fragment implements SearchMovie.onMovieSearchDone {

    ListView listaGlumci;
    MovieAdapter adapter;
    ArrayList<Movie> movies;
    onListItemClick oic;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search_glumci, container, false);

        listaGlumci = (ListView) view.findViewById(R.id.listaGlumci);
        movies = new ArrayList<>();
        adapter = new MovieAdapter(view.getContext(), movies);
        listaGlumci.setAdapter(adapter);

        oic = (onListItemClick) getActivity();

        final Button query = (Button) view.findViewById(R.id.query_button);
        final EditText editText = (EditText) view.findViewById(R.id.edit_query);

        query.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SearchMovie(SearchMovieFragment.this).execute(editText.getText().toString());
            }
        });

        listaGlumci.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                oic.onClick(movies.get(i).getNaziv());
            }
        });

        return view;
    }

    @Override
    public void onDone(ArrayList<Movie> rez) {
        movies.clear();
        movies.addAll(rez);
        adapter.notifyDataSetChanged();
    }

    public interface onListItemClick
    {
        void onClick(String imeFilma);
    }
}
