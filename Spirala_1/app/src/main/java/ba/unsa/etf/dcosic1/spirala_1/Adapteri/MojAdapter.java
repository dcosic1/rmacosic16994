package ba.unsa.etf.dcosic1.spirala_1.Adapteri;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ba.unsa.etf.dcosic1.spirala_1.Modeli.Glumac;
import ba.unsa.etf.dcosic1.spirala_1.R;


public class MojAdapter extends ArrayAdapter<Glumac> {
    private int resource;

    public MojAdapter(Context context, ArrayList<Glumac> objects){
        super(context, R.layout.element_liste_glumci, objects);
        this.resource= R.layout.element_liste_glumci;
        Context context1 = context;
        List<Glumac> listaGlumaca = objects;
    }

    public View getView(int position, View convertView, ViewGroup parent){

        LinearLayout newView;
        if (convertView == null) {

            newView = new LinearLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater li;
            li = (LayoutInflater)getContext().
                    getSystemService(inflater);
            li.inflate(resource, newView, true);
        } else {
            newView = (LinearLayout)convertView;
        }

        Glumac glumac = getItem(position);


        TextView naziv = (TextView) newView.findViewById(R.id.textViewNazivGlumca);
        TextView mjesto = (TextView) newView.findViewById(R.id.textViewMjestoGlumca);
        TextView godina = (TextView) newView.findViewById(R.id.textViewGodinaGlumca);
        TextView rating = (TextView) newView.findViewById(R.id.textViewRatingGlumca);
        ImageView image = (ImageView) newView.findViewById(R.id.imageView);


        naziv.setText(glumac.getIme());
        rating.setText(glumac.getRating());
        mjesto.setText(glumac.getMjestoRodjenja());
        godina.setText(glumac.getGodinaRodjenja());

        if (glumac.getIme().equals("Brad")) image.setImageResource(R.drawable.brad);
        else if (glumac.getIme().equals("Angelina")) image.setImageResource(R.drawable.angelina);
        else image.setImageResource(R.drawable.noimage);



        return newView;
    }



}