package ba.unsa.etf.dcosic1.spirala_1.Fragmenti;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import ba.unsa.etf.dcosic1.spirala_1.AsyncTaskovi.ActorDetails;
import ba.unsa.etf.dcosic1.spirala_1.AsyncTaskovi.MovieDetails;
import ba.unsa.etf.dcosic1.spirala_1.BazaHelperi.GlumacDBOpenHelper;
import ba.unsa.etf.dcosic1.spirala_1.Modeli.Glumac;
import ba.unsa.etf.dcosic1.spirala_1.Modeli.Movie;
import ba.unsa.etf.dcosic1.spirala_1.Modeli.Reziser;
import ba.unsa.etf.dcosic1.spirala_1.Modeli.Zanr;
import ba.unsa.etf.dcosic1.spirala_1.R;


public class GlumciDetaljiFragment extends Fragment implements ActorDetails.onArtistDone, MovieDetails.onMovieDone {

    private TextView ime;
    private TextView godine;
    private TextView link;
    private TextView bio;
    private TextView mjesto;
    private RelativeLayout rel;
    private Button podjeli;
    private ImageView image;
    private ImageView save;

    private Glumac glumac;
    private ArrayList<Reziser> reziseri;
    private ArrayList<Zanr> zanrovi;

    private View newView;
    private GlumacDBOpenHelper db;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {

       newView= inflater.inflate(R.layout.fragment_glumci_detalji, container, false);

        glumac = new Glumac();
        reziseri = new ArrayList<>();
        zanrovi= new ArrayList<>();

        ime = (TextView)newView.findViewById(R.id.imeGlumca);
        godine = (TextView)newView.findViewById(R.id.godina_rodjenja);
        link = (TextView)newView.findViewById(R.id.link);
        bio = (TextView)newView.findViewById(R.id.biografija);
        mjesto = (TextView)newView.findViewById(R.id.mjesto_rodjena);
        rel = (RelativeLayout)newView.findViewById(R.id.fragment_glumci_detalji);
        podjeli = (Button)newView.findViewById(R.id.podjeli);
        image = (ImageView)newView.findViewById(R.id.imageView2);
        save = (ImageView)newView.findViewById(R.id.save);

        db = new GlumacDBOpenHelper(newView.getContext());


        if (getArguments()!=null && getArguments().containsKey("glumac"))
        {
            int id = getArguments().getInt("glumac");
            Boolean baza = getArguments().getBoolean("baza");

            Glumac temp = db.getGlumac(id);
            if (temp==null)
                save.setImageDrawable(newView.getResources().getDrawable(R.drawable.empty));
            else save.setImageDrawable(newView.getResources().getDrawable(R.drawable.bookmark));


            if (baza)
            {
                glumac =db.getGlumac(id);
                ime.setText(glumac.getIme());
                godine.setText(glumac.getGodinaRodjenja());
                link.setText(glumac.getLink());
                bio.setText(glumac.getBiografija());
                mjesto.setText(glumac.getMjestoRodjenja());

                if (glumac.getSpol().equals("2")) rel.setBackgroundResource(R.color.colorPrimary);
                else rel.setBackgroundResource(R.color.colorAccent);
            }
            else
            new ActorDetails((ActorDetails.onArtistDone)GlumciDetaljiFragment.this).execute(id);

        }

        link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link.getText().toString()));
                startActivity(browserIntent);
            }
        });

        podjeli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, bio.getText().toString());
                sendIntent.setType("text/plain");
                startActivity(Intent.createChooser(sendIntent, getResources().getString(R.string.podjeli)));
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

           if (db.getGlumac(glumac.getId())==null)
           {
               long l = db.DodajGlumca(glumac);
               Toast.makeText(newView.getContext(), String.valueOf(l), Toast.LENGTH_LONG).show();
               for (int i=0; i<glumac.getMovieID().size(); i++)
               {
                   new MovieDetails(GlumciDetaljiFragment.this).execute(glumac.getMovieID().get(i));
               }
               save.setImageDrawable(newView.getResources().getDrawable(R.drawable.bookmark));
           }
           else
           {
               db.obrisiGlumca(glumac);
               save.setImageDrawable(newView.getResources().getDrawable(R.drawable.empty));

           }



            }
        });
        return newView;
    }


    @Override
    public void onDone(Glumac g) {
        glumac=g;

        Picasso.with(newView.getContext())
                .load("http://image.tmdb.org/t/p/w185//"+glumac.getSlika())
                .placeholder(R.drawable.noimage)
                .into(image);

        ime.setText(glumac.getIme());
        godine.setText(glumac.getGodinaRodjenja());
        link.setText(glumac.getLink());
        bio.setText(glumac.getBiografija());
        mjesto.setText(glumac.getMjestoRodjenja());

        if (glumac.getSpol().equals("2")) rel.setBackgroundResource(R.color.colorPrimary);
        else rel.setBackgroundResource(R.color.colorAccent);
    }

    @Override
    public void OnMovieDone(Movie m) {
        if (m.getReziser()!=null)
            if (!reziseri.contains(m.getReziser()))
            {
                db.dodajGlumacReziser(glumac, m.getReziser());
                reziseri.add(m.getReziser());
            }
        if (m.getZanr()!=null)
            if (!zanrovi.contains(m.getZanr()))
            {
                zanrovi.add(m.getZanr());
                db.dodajGlumacZanr(glumac, m.getZanr());
            }

    }
}
