package ba.unsa.etf.dcosic1.spirala_1;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;

import com.facebook.stetho.Stetho;

import ba.unsa.etf.dcosic1.spirala_1.Fragmenti.ButtonFragment;
import ba.unsa.etf.dcosic1.spirala_1.Fragmenti.ButtonHorizontalFragment;
import ba.unsa.etf.dcosic1.spirala_1.Fragmenti.FilmDetaljiFragment;
import ba.unsa.etf.dcosic1.spirala_1.Fragmenti.GlumciDetaljiFragment;
import ba.unsa.etf.dcosic1.spirala_1.Fragmenti.GlumciFragment;
import ba.unsa.etf.dcosic1.spirala_1.Fragmenti.ReziseriFragment;
import ba.unsa.etf.dcosic1.spirala_1.Fragmenti.SearchMovieFragment;
import ba.unsa.etf.dcosic1.spirala_1.Fragmenti.ZanroviFragment;
import ba.unsa.etf.dcosic1.spirala_1.Modeli.Glumac;

public class MainActivity extends AppCompatActivity implements GlumciFragment.onListItemClick,
        ButtonFragment.onGlumciClicked, ButtonFragment.onZanroviClicked,
        ButtonFragment.onReziseriClicked, ButtonHorizontalFragment.onOthersButtonClicked,
        ButtonHorizontalFragment.onGlumciButtonClicked, SearchMovieFragment.onListItemClick {

    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 3;
    private Glumac zadnji;

    private Boolean baza; //prikazuje li se iz baze ili ne
    //ne vidim bolje rješenje

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Stetho.initializeWithDefaults(this);
            zadnji=new Glumac();
            zadnji.setId(1);





        baza = false;

        FrameLayout f = (FrameLayout)findViewById(R.id.buttton_franme);

        if (f!=null)
        {
            getFragmentManager().beginTransaction().replace(R.id.buttton_franme, new ButtonFragment()).commit();
            getFragmentManager().beginTransaction().replace(R.id.lista_frame, new GlumciFragment()).commit();
        }

        else
        {
            getFragmentManager().beginTransaction().replace(R.id.lista_glumaca, new GlumciFragment()).commit();
            getFragmentManager().popBackStack(null,FragmentManager.POP_BACK_STACK_INCLUSIVE);

            Bundle arguments = new Bundle();
            arguments.putInt("glumac", zadnji.getId());
            arguments.putBoolean("baza", baza);
            GlumciDetaljiFragment fd =new GlumciDetaljiFragment();
            fd.setArguments(arguments);

            getFragmentManager().beginTransaction()
                    .replace(R.id.detalji_glumca, fd)
                    .addToBackStack(null)
                    .commit();

            ButtonHorizontalFragment bf = new ButtonHorizontalFragment();

            getFragmentManager().beginTransaction()
                    .replace(R.id.buttton_vert_frame, bf)
                    .addToBackStack(null)
                    .commit();
        }
    }


    @Override
    public void onListItemClicked(Glumac glumac, Boolean baza) {
        zadnji=glumac;
        this.baza=baza;
        Bundle arguments = new Bundle();
        arguments.putInt("glumac",glumac.getId());
        arguments.putBoolean("baza", baza);
        GlumciDetaljiFragment fd =new GlumciDetaljiFragment();
        fd.setArguments(arguments);

        FrameLayout f = (FrameLayout)findViewById(R.id.buttton_franme);

        if (f!=null)
        {
            getFragmentManager().beginTransaction()
                    .replace(R.id.lista_frame, fd)
                    .addToBackStack(null)
                    .commit();
        }
        else {
            getFragmentManager().beginTransaction()
                    .replace(R.id.detalji_glumca, fd)
                    .addToBackStack(null)
                    .commit();
        }
    }

    @Override
    public void onFilmoviClick() {
        FrameLayout f1 = (FrameLayout)findViewById(R.id.lista_frame);
        if(f1 != null)
        getFragmentManager().beginTransaction()
                .replace(R.id.lista_frame,new SearchMovieFragment())
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onGlumciClick() {
        FrameLayout f1 = (FrameLayout)findViewById(R.id.lista_frame);
        if(f1 != null)
            getFragmentManager().beginTransaction()
                    .replace(R.id.lista_frame,new GlumciFragment())
                    .addToBackStack(null)
                    .commit();
    }

    @Override
    public void onZanroviClick() {
        Bundle argumenti=new Bundle();
        argumenti.putInt("glumac", zadnji.getId());
        argumenti.putBoolean("baza", baza);

        ZanroviFragment zf = new ZanroviFragment();
        zf.setArguments(argumenti);
        getFragmentManager().beginTransaction()
                .replace(R.id.lista_frame, zf)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onReziseriClick() {
        Bundle argumenti=new Bundle();
        argumenti.putInt("glumac", zadnji.getId());
        argumenti.putBoolean("baza", baza);

        ReziseriFragment zf = new ReziseriFragment();
        zf.setArguments(argumenti);
        getFragmentManager().beginTransaction()
                .replace(R.id.lista_frame, zf)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void OnOtherButtonClick() {
        Bundle argumenti=new Bundle();
        argumenti.putInt("glumac", zadnji.getId());
        argumenti.putBoolean("baza", baza);

        ZanroviFragment zf = new ZanroviFragment();
        zf.setArguments(argumenti);
        getFragmentManager().beginTransaction()
                .replace(R.id.lista_glumaca, zf)
                .addToBackStack(null)
                .commit();

        ReziseriFragment rf = new ReziseriFragment();
        rf.setArguments(argumenti);
        getFragmentManager().beginTransaction()
                .replace(R.id.detalji_glumca, rf)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onGlumciButtonClick() {

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        GlumciFragment glumciFragment = new GlumciFragment();

        fragmentTransaction.replace(R.id.lista_glumaca, glumciFragment).commit();

        getFragmentManager().popBackStack(null,FragmentManager.POP_BACK_STACK_INCLUSIVE);

        Bundle arguments = new Bundle();
        arguments.putInt("glumac", zadnji.getId());
        arguments.putBoolean("baza", baza);
        GlumciDetaljiFragment fd =new GlumciDetaljiFragment();
        fd.setArguments(arguments);

        getFragmentManager().beginTransaction()
                .replace(R.id.detalji_glumca, fd)
                .addToBackStack(null)
                .commit();

        ButtonHorizontalFragment bf = new ButtonHorizontalFragment();

        getFragmentManager().beginTransaction()
                .replace(R.id.buttton_vert_frame, bf)
                .addToBackStack(null)
                .commit();
    }


    @Override
    public void onClick(String imeFilma) {
        FilmDetaljiFragment fd = new FilmDetaljiFragment();
        Bundle arguments = new Bundle();
        arguments.putString("naziv", imeFilma);
        fd.setArguments(arguments);
        getFragmentManager().beginTransaction()
                .replace(R.id.lista_frame, fd)
                .addToBackStack(null)
                .commit();
    }
}
