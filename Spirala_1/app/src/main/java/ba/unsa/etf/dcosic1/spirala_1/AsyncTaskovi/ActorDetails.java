package ba.unsa.etf.dcosic1.spirala_1.AsyncTaskovi;


import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import ba.unsa.etf.dcosic1.spirala_1.Modeli.Glumac;

public class ActorDetails  extends AsyncTask<Integer, Integer, Void> {

    public interface onArtistDone
    {
        void onDone (Glumac rez);
    }

    private ActorDetails.onArtistDone pozivatelj;

    public ActorDetails(ActorDetails.onArtistDone p) {pozivatelj=p;}

    private Glumac glumac;
    @Override
    protected Void doInBackground(Integer... integers) {

        String url1="https://api.themoviedb.org/3/person/"+integers[0]+"?api_key=3d8300df632dafd9b3bae72ed7fa946b&append_to_response=movie_credits";

        try {
            URL url = new URL(url1);
            HttpsURLConnection urlConnection = (HttpsURLConnection)url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String rezultat = convertInputToString(in);

            JSONObject jsonObject = new JSONObject(rezultat);
            glumac=new Glumac();
            glumac.setIme(jsonObject.getString("name"));
            glumac.setBiografija(jsonObject.getString("biography"));
            if (jsonObject.getString("birthday").length()>=4)
            glumac.setGodinaRodjenja(jsonObject.getString("birthday").substring(0,4));

//   TODO: ispitaj postoji li godina smrti
//         glumac.setGodinaSmrti(jsonObject.getString("deathday").substring(0,3));
            glumac.setSpol(String.valueOf(jsonObject.getString("gender")));
            glumac.setLink("https://www.themoviedb.org/person/"+integers[0]+"-"+glumac.getIme());
            glumac.setMjestoRodjenja(jsonObject.getString("place_of_birth"));
            glumac.setSlika(jsonObject.getString("profile_path"));
            glumac.setImdb(jsonObject.getInt("id"));
            glumac.setId(jsonObject.getInt("id"));
            JSONObject movieCredits = jsonObject.getJSONObject("movie_credits");
            JSONArray cast = movieCredits.getJSONArray("cast");

            List<Integer> movieID = new ArrayList<>();
            for (int i=0; i<cast.length(); i++)
            {
                JSONObject object = cast.getJSONObject(i);
                movieID.add(object.getInt("id"));
            }

            if (movieID.size()>8)

                movieID=movieID.subList(0, 8);

            glumac.setMovieID(movieID);



        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        pozivatelj.onDone(glumac);
    }

    private String convertInputToString(InputStream in)
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder sb = new StringBuilder();
        String line ;
        try {
            while ((line = reader.readLine())!=null)
            {
                sb.append(line).append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try
            {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}
