package ba.unsa.etf.dcosic1.spirala_1.Fragmenti;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

import ba.unsa.etf.dcosic1.spirala_1.Adapteri.MojAdapter;
import ba.unsa.etf.dcosic1.spirala_1.AsyncTaskovi.SearchActor;
import ba.unsa.etf.dcosic1.spirala_1.BazaHelperi.GlumacDBOpenHelper;
import ba.unsa.etf.dcosic1.spirala_1.Modeli.Glumac;
import ba.unsa.etf.dcosic1.spirala_1.R;


public class GlumciFragment extends Fragment implements SearchActor.onArtistSearchDone {

    private ArrayList<Glumac> glumci;
    private onListItemClick oic;
    private ListView listaGlumci;
    private GlumacDBOpenHelper db;

    private Boolean baza;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_glumci, container, false);

        glumci = new ArrayList<>();
        db = new GlumacDBOpenHelper(view.getContext());
        baza = false;

        listaGlumci = (ListView) view.findViewById(R.id.listaGlumci);

        oic = (onListItemClick) getActivity();


        final Button query = (Button) view.findViewById(R.id.query_button);
        final EditText editText = (EditText) view.findViewById(R.id.edit_query);
        final Button filmovi = (Button) view.findViewById(R.id.filmovi_search);

        filmovi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                oic.onFilmoviClick();
            }
        });

        query.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String queryText = editText.getText().toString();
                if (queryText.startsWith("actor:"))
                {
                    baza = true;
                    queryText=queryText.substring(6);
                    glumci = db.dajGlumca(queryText);
                    MojAdapter adapter = new MojAdapter(getActivity().getApplicationContext(), glumci);
                    listaGlumci.setAdapter(adapter);
                }
                else if (queryText.startsWith("director:"))
                {
                    queryText=queryText.substring(9);
                    glumci=db.dajGlumceRezisera(queryText);
                    MojAdapter adapter = new MojAdapter(getActivity().getApplicationContext(), glumci);
                    listaGlumci.setAdapter(adapter);
                }
                else
                {
                    baza=false;
                    new SearchActor( GlumciFragment.this).execute(queryText);
                }

            }
        });


        listaGlumci.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                oic.onListItemClicked(glumci.get(i), baza);
            }
        });
        return view;
    }

    @Override
    public void onDone(ArrayList<Glumac> rez) {
        glumci=rez;
        MojAdapter adapter = new MojAdapter(getActivity().getApplicationContext(), rez);
        listaGlumci.setAdapter(adapter);
    }

    public interface onListItemClick {
        void onListItemClicked(Glumac glumac, Boolean baza);
        void onFilmoviClick();
    }
}
