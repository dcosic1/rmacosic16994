package ba.unsa.etf.dcosic1.spirala_1.Adapteri;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ba.unsa.etf.dcosic1.spirala_1.R;
import ba.unsa.etf.dcosic1.spirala_1.Modeli.Zanr;


public class ZanrAdapter extends ArrayAdapter<Zanr> {
    private int resource;

    public ZanrAdapter(Context context, ArrayList<Zanr> objects){
        super(context, R.layout.element_liste_zanr, objects);
        this.resource= R.layout.element_liste_zanr;
        Context context1 = context;
        List<Zanr> listaZanrova = objects;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        LinearLayout newView;
        if (convertView == null) {

            newView = new LinearLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater li;
            li = (LayoutInflater) getContext().
                    getSystemService(inflater);
            li.inflate(resource, newView, true);
        } else {
            newView = (LinearLayout) convertView;
        }

        Zanr zanr = getItem(position);

        TextView naziv = (TextView) newView.findViewById(R.id.naziv_zanra);
        ImageView slika = (ImageView) newView.findViewById(R.id.zanr_slika);


        if (zanr != null) {
            naziv.setText(zanr.getNaziv());
        }
        if (zanr != null) {
            switch (zanr.getNaziv())
                 {

                     case "Action":
                         slika.setImageResource(R.drawable.action);
                         break;
                     case "Drama":
                         slika.setImageResource(R.drawable.drama);
                         break;
                     case "Comedy":
                         slika.setImageResource(R.drawable.comedy);
                         break;
                     case "Fantasy":
                         slika.setImageResource(R.drawable.fantasi);
                         break;
                     case "Crime":
                         slika.setImageResource(R.drawable.kimi);
                         break;
                     default:
                         slika.setImageResource(R.drawable.noimage);
                 }
        }

        return newView;
    }



}