package ba.unsa.etf.dcosic1.spirala_1;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class BiografijaGlumcaActivity extends AppCompatActivity {

    private String imePrezime;
    private String godina;
    private String link_;
    private String mjestoRodjenja;
    private String biografija;
    private String pol;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_biografija_glumca);

        Intent intent = getIntent();

        imePrezime=intent.getStringExtra("ime");
        godina=intent.getStringExtra("godina");
        link_=intent.getStringExtra("link");
        biografija=intent.getStringExtra("bio");
        mjestoRodjenja=intent.getStringExtra("mjesto");
        pol=intent.getStringExtra("pol");


        final TextView ime = (TextView)findViewById(R.id.imeGlumca);
        final TextView godine = (TextView)findViewById(R.id.godina_rodjenja);
        final TextView link = (TextView)findViewById(R.id.link);
        final TextView bio = (TextView)findViewById(R.id.biografija);
        final TextView mjesto = (TextView)findViewById(R.id.mjesto_rodjena);
        final RelativeLayout rel = (RelativeLayout)findViewById(R.id.glumac_detalji);
        final Button podjeli = (Button)findViewById(R.id.podjeli);
        final ImageView image = (ImageView)findViewById(R.id.imageView2);

        if (pol.equals("male")) rel.setBackgroundResource(R.color.colorPrimary);
        else rel.setBackgroundResource(R.color.colorAccent);

        ime.setText(imePrezime);
        godine.setText(godina);
        link.setText(link_);
        bio.setText(biografija);
        mjesto.setText(mjestoRodjenja);

        link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link.getText().toString()));
                startActivity(browserIntent);
            }
        });

        /*
        Koristeci ovaj intentChooser, uvijek nam se otvori dijalog u kojem možemo izabrati koju aplikaciju zelimo, cak i ako postoji
        samo jedna, te ako ne postoji nijedna, onda ispisuje odgovarajucu poruku, pa nema potrebe da provjeravamo ima li aplikacije
        koja bi mogla biti otvorena.

        (nađeno na developers.android.com)
        */
        podjeli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, bio.getText().toString());
                sendIntent.setType("text/plain");
                startActivity(Intent.createChooser(sendIntent, "Podijeli"));
            }
        });

        if (imePrezime.contains("Brad")) image.setImageResource(R.drawable.brad);
        else if (imePrezime.contains("Angelina")) image.setImageResource(R.drawable.angelina);
        else image.setImageResource(R.drawable.noimage);


    }
}
