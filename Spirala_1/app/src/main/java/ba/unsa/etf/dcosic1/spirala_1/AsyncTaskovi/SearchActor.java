package ba.unsa.etf.dcosic1.spirala_1.AsyncTaskovi;


import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

import ba.unsa.etf.dcosic1.spirala_1.Modeli.Glumac;

public class SearchActor extends AsyncTask<String, Integer, Void> {

    private ArrayList<Glumac> listaGlumci ;

    public interface onArtistSearchDone
    {
        void onDone (ArrayList<Glumac> rez);
    }

    private  onArtistSearchDone pozivatelj;

    public SearchActor(onArtistSearchDone p) {pozivatelj=p;}

    @Override
    protected Void doInBackground(String... strings) {
        listaGlumci=new ArrayList<>();
        String query = null;

        try {
            query = URLEncoder.encode(strings[0], "utf-8");
        }
       catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String url1 = "https://api.themoviedb.org/3/search/person?api_key=3d8300df632dafd9b3bae72ed7fa946b&query="+query;

        try
        {
            URL url = new  URL(url1);
            HttpsURLConnection urlConnection=(HttpsURLConnection)url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String rezultat = convertInputToString(in);

            JSONObject jsonObject = new JSONObject(rezultat);

            JSONArray glumci =jsonObject.getJSONArray("results");

            for (int i=0; i<glumci.length(); i++)
            {
                JSONObject glumacJson = glumci.getJSONObject(i);
                Glumac glumac = new Glumac();
                glumac.setIme(glumacJson.getString("name"));
                glumac.setRating((String.valueOf(glumacJson.getDouble("popularity"))));
                glumac.setId(glumacJson.getInt("id"));
                listaGlumci.add(glumac);
            }

        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        pozivatelj.onDone(listaGlumci);
    }

   private String convertInputToString(InputStream in)
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder sb = new StringBuilder();
        String line ;
        try {
            while ((line = reader.readLine())!=null)
            {
                sb.append(line).append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try
            {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}


