package ba.unsa.etf.dcosic1.spirala_1.Adapteri;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import ba.unsa.etf.dcosic1.spirala_1.Modeli.Movie;
import ba.unsa.etf.dcosic1.spirala_1.R;

public class MovieAdapter extends ArrayAdapter<Movie> {
    private int resource;
    public MovieAdapter(Context context, ArrayList<Movie> list) {
        super(context, R.layout.element_liste_reziseri, list);
        this.resource=R.layout.element_liste_reziseri;
    }

    public View getView(int position, View convertView, ViewGroup parent){

        LinearLayout newView;
        if (convertView == null) {

            newView = new LinearLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater li;
            li = (LayoutInflater)getContext().
                    getSystemService(inflater);
            li.inflate(resource, newView, true);
        } else {
            newView = (LinearLayout)convertView;
        }

       Movie zanr = getItem(position);

        TextView naziv = (TextView)newView.findViewById(R.id.reziser_naziv);


            naziv.setText(zanr.getNaziv());

        return newView;
    }
}
