package ba.unsa.etf.dcosic1.spirala_1.AsyncTaskovi;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import ba.unsa.etf.dcosic1.spirala_1.Modeli.Movie;
import ba.unsa.etf.dcosic1.spirala_1.Modeli.Reziser;
import ba.unsa.etf.dcosic1.spirala_1.Modeli.Zanr;


public class MovieDetails extends AsyncTask<Integer, Integer, Void> {

    public interface onMovieDone{
        void OnMovieDone(Movie m);
    }

    private onMovieDone pozivatelj;

    public MovieDetails(onMovieDone p )
    {
        this.pozivatelj=p;
    }

    private Movie m;
    @Override
    protected Void doInBackground(Integer... integers) {

        String url1 ="https://api.themoviedb.org/3/movie/"+integers[0]+"?api_key=3d8300df632dafd9b3bae72ed7fa946b&append_to_response=credits";
        try {
            URL url = new URL(url1);
            HttpsURLConnection urlConnection = (HttpsURLConnection)url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String rezultat = convertInputToString(in);

            JSONObject jsonObject = new JSONObject(rezultat);

            m = new Movie();

            m.setId(jsonObject.getInt("id"));
            m.setNaziv(jsonObject.getString("title"));

            JSONArray JSONzanr = jsonObject.getJSONArray("genres");

            if (JSONzanr.length()!=0)
            {
                Zanr zanr = new Zanr(JSONzanr.getJSONObject(0).getString("name"));
                zanr.setId(JSONzanr.getJSONObject(0).getInt("id"));
            }
                m.setZanr(new Zanr(JSONzanr.getJSONObject(0).getString("name")));

            JSONObject credita = jsonObject.getJSONObject("credits");
            JSONArray cast = credita.getJSONArray("crew");

            if (cast.length()!=0)
            {
                JSONObject reziser = cast.getJSONObject(0);
                if (!reziser.getString("name").equals(""))
                {
                    Reziser r = new Reziser(reziser.getString("name"));
                    r.setImdb(reziser.getInt("id"));
                    if (reziser.getString("job").equals("Director"))
                        m.setReziser(r);
                }


            }

        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }

        return null;
    }
    private String convertInputToString(InputStream in)
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder sb = new StringBuilder();
        String line ;
        try {
            while ((line = reader.readLine())!=null)
            {
                sb.append(line).append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try
            {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        pozivatelj.OnMovieDone(m);
    }
}
